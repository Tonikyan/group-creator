﻿using System;
using System.Collections.Generic;

namespace Mic.Homework.Grups
{
    static class DataBase
    {
        public static List<Student> CreateDataBass()
        {
            Random rd = new Random();
            List<Student> students = new List<Student>(10);
            for (int i = 0; i < students.Capacity; i++)
            {
                var st = new Student()
                {
                    Name = $"a{i}",
                    Surname = $"a{i}yan",
                    Age = rd.Next(7, 19)
                };
                students.Add(st);
            }
            return students;
        }
        public static void Print(List<Student> item)
        {
            foreach (var per in item)
            {
                Console.WriteLine($"**** Student ****");
                Console.WriteLine($"{per.FullName}");
                Console.WriteLine($"{per.Age}");
            }
        }
    }
}
