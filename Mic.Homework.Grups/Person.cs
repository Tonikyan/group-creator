﻿using System;
namespace Mic.Homework.Grups
{
    abstract class Person
    {
        public virtual int Age { get; set; }
        public  string Name { get; set; }
        public  string Surname { get; set; }
        public  string FullName => $"{Name} {Surname}";
    }
}
