﻿using System;
namespace Mic.Homework.Grups
{
    class Techer : Person
    {
        private int _age;
        public override int Age
        {
            get
            {
                if (_age < 25)
                    return 25;
                else
                    return _age;
            }
            set
            {
                _age = value;
            }
        }
    }
}
